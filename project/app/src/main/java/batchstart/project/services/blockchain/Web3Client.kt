package batchstart.project.services.blockchain

import batchstart.project.services.blockchain.contracts.Migrations_sol_Migrations
import batchstart.project.utils.CONTACT_ADDRESS
import batchstart.project.utils.GAS_LIMIT
import batchstart.project.utils.GAS_PRICE
import batchstart.project.utils.ROPSTEN_ENDPOINT
import org.web3j.crypto.Credentials
import org.web3j.protocol.Web3j
import org.web3j.protocol.http.HttpService

class Web3Client {
    private val web3 = Web3j.build(HttpService(ROPSTEN_ENDPOINT))

    fun loadMigrationsContract(credentials: Credentials): Migrations_sol_Migrations {
        return Migrations_sol_Migrations.load(CONTACT_ADDRESS, web3, credentials, GAS_LIMIT, GAS_PRICE )
    }
}