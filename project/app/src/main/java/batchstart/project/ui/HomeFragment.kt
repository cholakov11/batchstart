package batchstart.project.ui

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import batchstart.project.R
import kotlinx.android.synthetic.main.home_fragment.*
import org.web3j.crypto.Credentials
import java.util.jar.Manifest

class HomeFragment: Fragment() {

    private lateinit var parent: HostActivityInterface

    private val TAG = "HomeFragment"

    override fun onAttach(context: Context) {
        super.onAttach(context)
        parent = context as HostActivityInterface
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        if(parent.getHostViewModel().checkStoragePermission()) {
            setGoButton()
        } else {
            Log.d(TAG,"No permission. Requesting...")
            requestPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        Log.d(TAG, "Invoked")
        when(requestCode) {
            1 -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d(TAG, "Button will be set...")
                    setGoButton()
                } else {
                    Log.d(TAG, "Permission denied")
                }
                return
            }
            else -> {
            }
        }
    }

    private fun setGoButton() {
        go.setOnClickListener { v ->
            if(parent.getHostViewModel().checkForWallet()) {
                parent.getHostViewModel().loadWallet(password.text.toString())
            } else {
                parent.getHostViewModel().createWallet(password.text.toString())
            }
        }
        Log.d(TAG, "Button is set")
    }

    private fun observeLiveData() {
        parent.getHostViewModel().credentials.observe(this.viewLifecycleOwner, Observer<Credentials> { credentials ->
            if(credentials != null) {
                Log.d(TAG, "Credentials obtained navigating to contract screen!")
                findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToContractFragment())
            }
        })

    }
}