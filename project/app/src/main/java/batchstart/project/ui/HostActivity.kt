package batchstart.project.ui

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import batchstart.project.R
import batchstart.project.viewmodel.HostViewModel
import batchstart.project.viewmodel.HostViewModelFactory
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.security.Security

class HostActivity: AppCompatActivity(), HostActivityInterface {

    private val viewModel: HostViewModel by viewModels { HostViewModelFactory(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.host_activity)
        setUpBouncyCastle()
    }

    override fun getHostViewModel(): HostViewModel {
        return this.viewModel
    }

    private fun setUpBouncyCastle() {
        val provider = Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) ?: return

        if(provider.javaClass.equals(BouncyCastleProvider::class.java)) {
            return
        }

        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME)
        Security.insertProviderAt(BouncyCastleProvider(), 1)
    }
}