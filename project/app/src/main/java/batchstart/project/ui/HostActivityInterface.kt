package batchstart.project.ui

import batchstart.project.viewmodel.HostViewModel

interface HostActivityInterface {
    fun getHostViewModel(): HostViewModel
}