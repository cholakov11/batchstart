package batchstart.project.viewmodel

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import batchstart.project.services.blockchain.Web3Client
import batchstart.project.services.blockchain.contracts.Migrations_sol_Migrations
import batchstart.project.services.blockchain.wallet.WalletCreator
import batchstart.project.utils.CONTACT_ADDRESS
import org.web3j.crypto.Credentials
import java.math.BigInteger
import java.util.concurrent.Executors

class HostViewModel(private val activity: AppCompatActivity): ViewModel() {
    private val web3Client: Web3Client = Web3Client()
    private val walletCreator = WalletCreator(activity)
    lateinit var contract: Migrations_sol_Migrations


    val credentials: MutableLiveData<Credentials> = MutableLiveData()
    val owner: MutableLiveData<String> = MutableLiveData()
    val lastMigration: MutableLiveData<BigInteger> = MutableLiveData()

    fun checkForWallet(): Boolean {
        return walletCreator.hasWallet()
    }

    fun createWallet(pass: String) {
        walletCreator.generateWallet(pass) {
            loadWallet(pass)
        }
    }

    fun loadWallet(pass: String) {
        walletCreator.loadCredentials(pass) {
            credentials.value = it
            loadContract(it)
        }
    }

    fun loadContract(credentials: Credentials) {
        contract = web3Client.loadMigrationsContract(credentials)
    }

    fun getOwnerFromContract() {
        Executors.newSingleThreadExecutor().execute {
            val o = contract.owner().send()
            owner.postValue(o)
        }
    }

    fun getLastMigration() {
        Executors.newSingleThreadExecutor().execute {
            val m = contract.last_completed_migration().send()
            lastMigration.postValue(m)
        }
    }

    fun checkStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(activity,"Permission is needed to check for existing wallet or generate one",Toast.LENGTH_LONG).show()
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

    }
}